# Actors
![pipeline-badge](https://gitlab.com/bbmsoft.net/actors/badges/develop/pipeline.svg)

A minimal library for building applications modeled after the core ideas of the actor system without having to commit to a full-fledged actor framework like Akka.