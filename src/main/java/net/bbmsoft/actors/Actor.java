package net.bbmsoft.actors;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

/**
 * <p>
 * The Actor class provides useful functions for modeling applications after the
 * core ideas of the actor system without having to commit to a full-fledged
 * framework like Akka.
 * </p>
 * <p>
 * The general idea of an actor system is to encapsulate state in a stricter way
 * than object oriented programming alone usually does, thus avoiding data races
 * and corruption while also decoupling actors for easier modularization and
 * parallel execution that does not deadlock. The core purpose of an actor is
 * actually very similar to what object oriented programming was originally
 * designed for:<br>
 * Tying a coherent piece of application state to the minimal amount of
 * application logic that is required to safely access and manage that
 * state.<br>
 * As such an actor that contains only application logic without any internal
 * state does not serve its purpose and should rather be implemented as a
 * functional service.
 * </p>
 * <p>
 * The most important rules of any actor system are:
 * </p>
 * <ul>
 * <li>Actors are allowed to:
 * <ol>
 * <li>spawn new actors</li>
 * <li>kill actors they spawned</li>
 * <li>send messages* to other actors, possibly containing copies of (parts of)
 * their internal state</li>
 * <li>receive messages from other actors and react to them by updating their
 * own internal state or spawning/killing child actors or commit suicide</li>
 * </ol>
 * </li>
 * <li>Actors are NOT allowed to:
 * <ol>
 * <li>kill actors they didn't spawn</li>
 * <li>expose mutable state to other actors (or any other parts of the
 * application)</li>
 * <li>directly mutate any state other than their own</li>
 * </ol>
 * </li>
 * <li>Actors should be single threaded. In some cases running some tasks on a
 * background thread may be preferable to delegating them to an external
 * executor service (for example when calling untrusted listeners that might
 * block or otherwise violate rules of a proper actor system), however
 * background tasks MUST NEVER change the actor's state. ANY changes of the
 * actors state must be done on its main thread.</li>
 * <li>Accordingly whenever an Actor receives a message it MUST NOT process it
 * on the calling thread, but must delegate it to the Actor's internal thread as
 * soon as possible.</li>
 * <li>Usually an actor's main thread should never block, especially not for
 * synchronization purposes. If you need {@code synchronized} blocks,
 * {@link java.util.concurrent.locks.ReentrantLock ReentrantLocks},
 * {@link java.util.concurrent.CountDownLatch CountDownLatches} etc. your design
 * is most probably flawed. Cases where blocking can be acceptable include I/O
 * operations or processing events from a {@link BlockingQueue}. However even in
 * these cases the blocking should be either interruptible or timeouts should be
 * used to make sure the actor stays responsive, e.g. in case someone is trying
 * to ask it to shut down or stop what it's currently doing.</li>
 * </ul>
 * *Since this implementation of an actor system is meant to cause minimal
 * overhead, providing a full messaging system like some other frameworks do is
 * out of scope. Instead, "sending a message" to another actor is achieved by
 * calling a method of its public API. This requires any public methods on an
 * actor to follow these rules:
 * <ul>
 * <li>the method MUST NOT have a return value</li>
 * <li>the method MUST NOT block and should ideally not do anything else than
 * delegating to a private method that is being run on the receiving actor's
 * internal thread. It may however be desirable to perform precondition checks
 * on the method's input variables on the caller thread so that appropriate
 * exceptions can be propagated to the caller</li>
 * <li>the method can have any number of arguments, however arguments passed to
 * the method MUST NOT contain any references to the sending actor itself or any
 * of its internal state, i.e. even if the receiver decides to keep the message
 * forever that must not prevent the actor that sent it from being garbage
 * collected</li>
 * </ul>
 * <p>
 * ANY method in an actor's public API should comply with these rules. If there
 * needs to be an exception, that method's documentation must make it very clear
 * that it must not be called directly by any other actor.
 * </p>
 * <p>
 * When migrating an existing application to an actor system, some of these
 * rules may be loosened up a bit, to avoid having to make too radical changes:
 * </p>
 * <ul>
 * <li>It can be OK for actors to expose their internal state, as long as it
 * happens in a thread-safe, non-blocking and immutable way. Here's an example:
 *
 * <pre>
public class MyActor extends Actor {

	...

	// notice that the items field is final and references a thread-safe, lock-free list implementation,
	// so external access can cause neither data races nor deadlocks
	private final List&lt;Item&gt; items = new CopyOnWriteArrayList&lt;&gt;();

	// calling this message is equivalent to sending the actor a message, which is perfectly legal
	public void addItem(Item item) {
		// actor's internal state is updated on its own main thread
		this.execute(() -&gt; {
			this.items.add(item);
		});
	}

	// calling this method is NOT equivalent to sending the actor a message (because it directly returns
	// something) and is thus technically not allowed. In this case it can be tolerated because returning
	// a read-only copy of the actor's internal state IS equivalent to sending a message back to the caller
	// that does not conflict with any of the aforementioned rules
	public void getItems() {
		List&lt;Item&gt; copy = new ArrayList&lt;&gt;(this.items);
		return Collections.unmodifiableList(copy);
	}

	...
}
 * </pre>
 *
 * </li>
 * </ul>
 * <p>
 * An actor should keep it's entire state within one class. Spreading the state
 * across multiple classes is technically possible, but it makes the concept of
 * an encapsulated entity less tangible and opens the door for error when
 * implementing the rules the actor needs to comply with.<br>
 * Moving (state-less) functionality out of an actor's main class is perfectly
 * fine and in fact even encouraged. Any functionality that does not rely on
 * access to an actor's internal state should be provided in a way that allows
 * it to be shared and reused, for example as an independent functional service.
 * </p>
 * <p>
 * Actors can be implemented either by deriving the actor's main class from
 * {@code com.salzbrenner.actors.Actor} or by giving it a final Actor field. The
 * first method is generally encouraged whenever possible, however sometimes an
 * actor's main class needs to be derived from some other class and thus cannot
 * also inherit from Actor.
 * </p>
 * 
 * @author mbachmann
 *
 */
public class Actor implements AutoCloseable, Executor, Logger {

	private volatile static boolean performThreadCheck = true;

	/**
	 * Check whether {@link Actor#assertActorThread()} actually does something.
	 *
	 * @return {@code true} if {@link Actor#assertActorThread() assertActorThread}
	 *         is configured to do something, {@code false} otherwise
	 * @see Actor#setPerformThreadCheck(boolean)
	 * @see Actor#assertActorThread()
	 */
	public static boolean isPerformThreadCheck() {
		return Actor.performThreadCheck;
	}

	/**
	 * Defines the behavior of {@link Actor#assertActorThread()}. When set to
	 * {@code true}, {@link Actor#assertActorThread() assertActorThread} will throw
	 * an {@link IllegalStateException} when called from any other than the actor's
	 * own main thread, if set to {@code false}, {@link Actor#assertActorThread()
	 * assertActorThread} is a no-op.
	 *
	 * @param performThreadCheck whether or not {@link Actor#assertActorThread()
	 *                           assertActorThread} does something
	 * @see Actor#assertActorThread()
	 */
	public static void setPerformThreadCheck(final boolean performThreadCheck) {
		Actor.performThreadCheck = performThreadCheck;
	}

	public final Logger log;

	private final Class<?> clazz;

	private final ScheduledExecutorService executor;
	private final Executor dispatcher;
	private final ThreadPoolExecutor backgroundExecutor;
	private final Map<UUID, Future<?>> futures;
	private final String name;
	private final Queue<Runnable> cleanupTasks;

	private volatile boolean closed;
	private volatile Thread actorThread;

	/**
	 * Default constructor for classes that are derived from Actor. Creates a logger
	 * for the actual runtime type of the extending class.
	 *
	 * @param name the actor's name; used to label the actor's thread(s) and for
	 *             logging. Must not be {@code null}
	 * @throws NullPointerException if {@code name} is {@code null}
	 */
	protected Actor(final String name) {
		this(name, null, null, null);
	}

	/**
	 * Alternative constructor for classes that are derived from Actor that allows
	 * using a custom internal executer. Creates a logger for the actual runtime
	 * type of the extending class.
	 * <p>
	 * Notice that the provided dispatcher MUST delegate execution to a single
	 * thread!
	 *
	 * @param name       the actor's name; used to label the actor's thread(s) and
	 *                   for logging. Must not be {@code null}
	 * @param dispatcher internal executor providing the actor's main thread. Must
	 *                   delegate tasks to a single thread. Must not be {@code null}
	 * @throws NullPointerException if {@code name} or {@code executor} are
	 *                              {@code null}
	 */
	protected Actor(final String name, final Executor dispatcher) {
		this(Objects.requireNonNull(name), Objects.requireNonNull(dispatcher), null, null);
	}

	/**
	 * Default constructor for classes that already extend a different class and
	 * thus need to reference an Actor instance as a field. Creates a logger for the
	 * provided class.
	 *
	 * @param name  the actor's name; used to label the actor's thread(s) and for
	 *              logging. Must not be {@code null}
	 * @param clazz type of the actual actor main class. A logger will be created
	 *              from this type. Must not be {@code null}
	 * @throws NullPointerException if {@code name} or {@code clazz} are
	 *                              {@code null}
	 */
	public Actor(final String name, final Class<?> clazz) {
		this(Objects.requireNonNull(name), null, Objects.requireNonNull(clazz), null);
	}

	/**
	 * Alternative constructor for classes that that already extend a different
	 * class that allows using a custom internal executer. Creates a logger for the
	 * provided class.
	 * <p>
	 * Notice that the provided dispatcher MUST delegate execution to a single
	 * thread!
	 *
	 * @param name       the actor's name; used to label the actor's thread(s) and
	 *                   for logging. Must not be {@code null}
	 * @param dispatcher internal executor providing the actor's main thread. Must
	 *                   delegate tasks to a single thread. Must not be {@code null}
	 * @param clazz      type of the actual actor main class. A logger will be
	 *                   created from this type. Must not be {@code null}
	 * @throws NullPointerException if {@code name} or {@code executor} are
	 *                              {@code null}
	 */
	protected Actor(final String name, final Executor dispatcher, final Class<?> clazz) {
		this(Objects.requireNonNull(name), Objects.requireNonNull(dispatcher), Objects.requireNonNull(clazz), null);
	}

	private Actor(final String name, final Executor dispatcher, final Class<?> clazz, final Object phantomData) {
		this.clazz = clazz != null ? clazz : this.getClass();
		this.name = Objects.requireNonNull(name);
		this.log = LoggerFactory.getLogger(this.clazz);
		this.executor = Executors.newSingleThreadScheduledExecutor(r -> new Thread(r, name));
		this.dispatcher = dispatcher != null ? dispatcher : Runnable::run;
		this.backgroundExecutor = this.createBackgroundExecutor(name);
		this.futures = new ConcurrentHashMap<>();
		this.cleanupTasks = new LinkedBlockingQueue<>();

		this.execute(() -> this.actorThread = Thread.currentThread());
	}

	/**
	 * Queues a {@link Runnable} for execution as part of the Actor's shutdown
	 * routine. Tasks will be stored in a FIFO queue and executed in order on the
	 * Actor's main thread, when {@link Actor#close()} is called.
	 * <p>
	 * Tasks executed via this function can safely access and modify the Actor's
	 * internal state.
	 *
	 * @param cleanupTask task to be run during the Actor's shutdown process. Must
	 *                    not be {@code null}
	 * @throws NullPointerException if {@code cleanupTask} is {@code null}
	 */
	public void runOnClose(final Runnable cleanupTask) {
		Objects.requireNonNull(cleanupTask);
		Runnable safeTask = this.safelyDispatch(cleanupTask, null);
		this.cleanupTasks.add(safeTask);
	}

	/**
	 * Executes a task on the Actor's main thread. Tasks will be executed in the
	 * order in which they are submitted and cannot be cancelled.
	 * <p>
	 * Tasks executed via this function can safely access and modify the Actor's
	 * internal state.
	 *
	 * @param task will be executed on the Actor's main thread. Must not be
	 *             {@code null}
	 * @throws NullPointerException if {@code task} is {@code null}
	 * @see Executor#execute(Runnable)
	 */
	@Override
	public void execute(final Runnable task) {
		this.doSubmit(Objects.requireNonNull(task), false);
	}

	/**
	 * Executes a task on the Actor's main thread. Tasks will be executed in the
	 * order in which they are submitted and can be cancelled by calling
	 * {@link #cancel(UUID)} with the UUID returned by this function. Canceling the
	 * task is only possible as long as it has not already been started.
	 * <p>
	 * When {@link #close()} is called, any pending tasks will automatically be
	 * cancelled.
	 * <p>
	 * Tasks executed via this function can safely access and modify the Actor's
	 * internal state.
	 *
	 * @param task will be executed on the Actor's main thread. Must not be
	 *             {@code null}
	 * @return a UUID that can be used to {@link #cancel(UUID) cancel} the task.
	 * @throws NullPointerException if {@code task} is {@code null}
	 * @see ExecutorService#submit(Runnable)
	 */
	public UUID submit(final Runnable task) {
		return this.doSubmit(Objects.requireNonNull(task), true);
	}

	/**
	 * Executes a task on a random background thread. Tasks are not guaranteed to be
	 * executed in the order in which they are submitted and cannot be cancelled.
	 * <p>
	 * Tasks executed via this function can NOT safely access or modify the Actor's
	 * internal state and are thus forbidden to do so by convention.
	 *
	 * @param task will be executed on a randomly selected background thread. Must
	 *             not be {@code null}
	 * @throws NullPointerException if {@code task} is {@code null}
	 */
	public void runInBackground(final Runnable task) {

		if (this.closed) {
			return;
		}

		Runnable safeTask = this.safelyWrap(Objects.requireNonNull(task));

		this.backgroundExecutor.execute(safeTask);
	}

	/**
	 * Executes a task on the Actor's main thread once after the given delay.
	 * Execution of the task can be cancelled by calling {@link #cancel(UUID)} with
	 * the UUID returned by this function. Canceling the task is only possible as
	 * long as it has not already been started.
	 * <p>
	 * When {@link #close()} is called, any pending tasks will automatically be
	 * cancelled.
	 * <p>
	 * Tasks executed via this function can safely access and modify the Actor's
	 * internal state.
	 *
	 * @param task  will be executed on the Actor's main thread. Must not be
	 *              {@code null}
	 * 
	 * @param delay time to wait before executing {@code task}
	 * @param unit  time unit of {@code delay}
	 * @return a UUID that can be used to {@link #cancel(UUID) cancel} the task.
	 * @throws NullPointerException if {@code task} is {@code null}
	 * @see ScheduledExecutorService#schedule(java.util.concurrent.Callable, long,
	 *      TimeUnit)
	 */
	public UUID schedule(final Runnable task, final long delay, final TimeUnit unit) {

		if (this.closed) {
			return null;
		}

		UUID uuid = UUID.randomUUID();
		Runnable safeTask = this.safelyDispatch(Objects.requireNonNull(task), uuid);

		this.doSchedule(delay, unit, safeTask, uuid);

		return uuid;
	}

	/**
	 * Executes a task on the Actor's main thread repeatedly at the given rate.
	 * Execution of the task can be cancelled by calling {@link #cancel(UUID)} with
	 * the UUID returned by this function. Canceling the task is only possible as
	 * long as it has not already been started.
	 * <p>
	 * When {@link #close()} is called, any pending tasks will automatically be
	 * cancelled.
	 * <p>
	 * Tasks executed via this function can safely access and modify the Actor's
	 * internal state.
	 *
	 * @param task         will be executed on the Actor's main thread. Must not be
	 *                     {@code null}
	 * 
	 * @param initialDelay time to wait before first executions of {@code task}
	 * @param delay        time to wait between executions of {@code task}
	 * @param unit         time unit of {@code delay}
	 * @return a UUID that can be used to {@link #cancel(UUID) cancel} the task.
	 * @throws NullPointerException if {@code task} is {@code null}
	 * @see ScheduledExecutorService#scheduleAtFixedRate(Runnable, long, long,
	 *      TimeUnit)
	 */
	public UUID scheduleAtFixedRate(final Runnable task, final long initialDelay, final long delay,
			final TimeUnit unit) {

		if (this.closed) {
			return null;
		}

		UUID uuid = UUID.randomUUID();
		Runnable safeTask = this.safelyDispatch(Objects.requireNonNull(task), null);

		this.doScheduleAtFixedRate(initialDelay, delay, unit, safeTask, uuid);

		return uuid;
	}

	/**
	 * Executes a task on the Actor's main thread repeatedly with the given delay in
	 * between executions. Execution of the task can be cancelled by calling
	 * {@link #cancel(UUID)} with the UUID returned by this function. Canceling the
	 * task is only possible as long as it has not already been started.
	 * <p>
	 * When {@link #close()} is called, any pending tasks will automatically be
	 * cancelled.
	 * <p>
	 * Tasks executed via this function can safely access and modify the Actor's
	 * internal state.
	 *
	 * @param task         will be executed on the Actor's main thread. Must not be
	 *                     {@code null}
	 * 
	 * @param initialDelay time to wait before first executions of {@code task}
	 * @param delay        time to wait between executions of {@code task}
	 * @param unit         time unit of {@code delay}
	 * @return a UUID that can be used to {@link #cancel(UUID) cancel} the task.
	 * @throws NullPointerException if {@code task} is {@code null}
	 * @see ScheduledExecutorService#scheduleWithFixedDelay(Runnable, long, long,
	 *      TimeUnit)
	 */
	public UUID scheduleWithFixedDelay(final Runnable task, final long initialDelay, final long delay,
			final TimeUnit unit) {

		if (this.closed) {
			return null;
		}

		UUID uuid = UUID.randomUUID();
		Runnable safeTask = this.safelyDispatch(Objects.requireNonNull(task), null);

		this.doScheduleWithFixedDelay(initialDelay, delay, unit, safeTask, uuid);

		return uuid;
	}

	/**
	 * Checks whether this function is called on the Actor's main thread, unless
	 * {@link Actor#setPerformThreadCheck(boolean)} has been set to {@code false}.
	 * <p>
	 * The default behavior is to throw an {@link IllegalStateException} if this
	 * function is called from any other than the Actor's main thread. When
	 * {@link Actor#setPerformThreadCheck(boolean) setPerformThreadCheck(false)} has
	 * been called, this is a no-op.
	 * <p>
	 * This method is meant for asserting that all Actors behave correctly during
	 * development. In a production system the checks can be disabled to avoid the
	 * overhead of constantly checking on which thread something is running.
	 * <p>
	 * A typical usage example would look like this:
	 *
	 * <pre>
	public class MyActor extends Actor {
	
		...
	
		// public API methods should immediately delegate to the actor thread
		public void update() {
			this.execute(this::doUpdate);
		}
	
		// private API should be able to safely assume it is allowed to mutate the actor's state,
		// so assertActorThread() is used to detect any illegal invocations during development
		void doUpdate() {
			this.assertActorThread();
			// actually perform any necessary state mutations
			...
		}
	
		...
	}
	 * </pre>
	 */
	public void assertActorThread() {

		if (!Actor.performThreadCheck) {
			return;
		}

		Thread currentThread = Thread.currentThread();

		if (currentThread != this.actorThread) {
			throw new AssertionError("Not on main thread of actor '" + this.name + "'. Current thread is '"
					+ currentThread.getName() + "'");
		}
	}

	/**
	 * Cancel a task submitted via {@link #submit(Runnable)},
	 * {@link #schedule(Runnable, long, TimeUnit)},
	 * {@link #doScheduleAtFixedRate(long, long, TimeUnit, Runnable, UUID)} or
	 * {@link #doScheduleWithFixedDelay(long, long, TimeUnit, Runnable, UUID)}.
	 * <p>
	 * Canceling the task is only possible if it hasn't already started running.
	 *
	 * @param uuid id of the task to be canceled
	 */
	public void cancel(final UUID uuid) {

		if (this.closed) {
			return;
		}

		if (uuid == null) {
			return;
		}

		Future<?> future = this.futures.remove(uuid);
		if (future != null) {
			future.cancel(false);
		}
	}

	/**
	 * Close this actor. This will perform the following steps:
	 * <ol>
	 * <li>prevent any new tasks to be scheduled via
	 * <ul>
	 * <li>{@link #execute(Runnable) execute}</li>
	 * <li>{@link #runInBackground(Runnable) runInBackground}</li>
	 * <li>{@link #submit(Runnable) submit}</li>
	 * <li>{@link #schedule(Runnable, long, TimeUnit) schedule}</li>
	 * <li>{@link #scheduleAtFixedRate(Runnable, long, long, TimeUnit)
	 * scheduleAtFixedRate}</li>
	 * <li>{@link #scheduleWithFixedDelay(Runnable, long, long, TimeUnit)
	 * scheduleWithFixedDelay}</li>
	 * </ul>
	 * </li>
	 * <li>cancel all pending tasks that were submitted via
	 * <ul>
	 * <li>{@link #submit(Runnable) submit}</li>
	 * <li>{@link #schedule(Runnable, long, TimeUnit) schedule}</li>
	 * <li>{@link #scheduleAtFixedRate(Runnable, long, long, TimeUnit)
	 * scheduleAtFixedRate}</li>
	 * <li>{@link #scheduleWithFixedDelay(Runnable, long, long, TimeUnit)
	 * scheduleWithFixedDelay}</li>
	 * </ul>
	 * </li>
	 * <li>execute all tasks scheduled via {@link #runOnClose(Runnable)
	 * runOnClose}</li>
	 * <li>shut down all internal threads and executors</li>
	 * </ol>
	 * Once this method has returned, this Actor is dead and cannot be reused.
	 */
	@Override
	public void close() {
		this.closed = true;
		this.futures.values().forEach(f -> f.cancel(false));
		this.cleanupTasks.forEach(task -> this.doRun(task, null));
		this.shutdown(this.backgroundExecutor);
		this.shutdown(this.executor);
	}

	/**
	 * Returns whether or not this Actor has been closed.
	 *
	 * @return {@code true} if the actor has been closed, {@code false} otherwise
	 */
	public boolean isClosed() {
		return this.closed;
	}

	/**
	 * Interrupts the actor thread. Since {@link #cancel(UUID)} can only be used to
	 * abort the execution of tasks that haven't started running yet, it may
	 * sometimes be necessary to interrupt the Actor's main thread to stop a long
	 * running task that is already executing.
	 * <p>
	 * Tasks that block or take a long time to execute should normally not be run on
	 * an Actor thread, so if you feel like you need to use this method, you may
	 * want to re-think your design decisions. Generally, do not call this method if
	 * you haven't very carefully evaluated what you are doing and are sure it is
	 * correct.
	 */
	public void interruptActorThread() {

		Thread actorThread = this.actorThread;

		if (actorThread != null) {
			actorThread.interrupt();
		}
	}

	@Override
	public String getName() {
		return this.log.getName();
	}

	@Override
	public boolean isTraceEnabled() {
		return this.log.isTraceEnabled();
	}

	@Override
	public void trace(final String msg) {
		this.log.trace(msg);
	}

	@Override
	public void trace(final String format, final Object arg) {
		this.log.trace(format, arg);
	}

	@Override
	public void trace(final String format, final Object arg1, final Object arg2) {
		this.log.trace(format, arg1, arg2);
	}

	@Override
	public void trace(final String format, final Object... arguments) {
		this.log.trace(format, arguments);
	}

	@Override
	public void trace(final String msg, final Throwable t) {
		this.log.trace(msg, t);
	}

	@Override
	public boolean isTraceEnabled(final Marker marker) {
		return this.log.isTraceEnabled(marker);
	}

	@Override
	public void trace(final Marker marker, final String msg) {
		this.log.trace(marker, msg);
	}

	@Override
	public void trace(final Marker marker, final String format, final Object arg) {
		this.log.trace(marker, format, arg);
	}

	@Override
	public void trace(final Marker marker, final String format, final Object arg1, final Object arg2) {
		this.log.trace(marker, format, arg1, arg2);
	}

	@Override
	public void trace(final Marker marker, final String format, final Object... argArray) {
		this.log.trace(marker, format, argArray);
	}

	@Override
	public void trace(final Marker marker, final String msg, final Throwable t) {
		this.log.trace(marker, msg, t);
	}

	@Override
	public boolean isDebugEnabled() {
		return this.log.isDebugEnabled();
	}

	@Override
	public void debug(final String msg) {
		this.log.debug(msg);
	}

	@Override
	public void debug(final String format, final Object arg) {
		this.log.debug(format, arg);
	}

	@Override
	public void debug(final String format, final Object arg1, final Object arg2) {
		this.log.debug(format, arg1, arg2);
	}

	@Override
	public void debug(final String format, final Object... arguments) {
		this.log.debug(format, arguments);
	}

	@Override
	public void debug(final String msg, final Throwable t) {
		this.log.debug(msg, t);
	}

	@Override
	public boolean isDebugEnabled(final Marker marker) {
		return this.log.isDebugEnabled(marker);
	}

	@Override
	public void debug(final Marker marker, final String msg) {
		this.log.debug(marker, msg);
	}

	@Override
	public void debug(final Marker marker, final String format, final Object arg) {
		this.log.debug(marker, format, arg);
	}

	@Override
	public void debug(final Marker marker, final String format, final Object arg1, final Object arg2) {
		this.log.debug(marker, format, arg1, arg2);
	}

	@Override
	public void debug(final Marker marker, final String format, final Object... arguments) {
		this.log.debug(marker, format, arguments);
	}

	@Override
	public void debug(final Marker marker, final String msg, final Throwable t) {
		this.log.debug(marker, msg, t);
	}

	@Override
	public boolean isInfoEnabled() {
		return this.log.isInfoEnabled();
	}

	@Override
	public void info(final String msg) {
		this.log.info(msg);
	}

	@Override
	public void info(final String format, final Object arg) {
		this.log.info(format, arg);
	}

	@Override
	public void info(final String format, final Object arg1, final Object arg2) {
		this.log.info(format, arg1, arg2);
	}

	@Override
	public void info(final String format, final Object... arguments) {
		this.log.info(format, arguments);
	}

	@Override
	public void info(final String msg, final Throwable t) {
		this.log.info(msg, t);
	}

	@Override
	public boolean isInfoEnabled(final Marker marker) {
		return this.log.isInfoEnabled(marker);
	}

	@Override
	public void info(final Marker marker, final String msg) {
		this.log.info(marker, msg);
	}

	@Override
	public void info(final Marker marker, final String format, final Object arg) {
		this.log.info(marker, format, arg);
	}

	@Override
	public void info(final Marker marker, final String format, final Object arg1, final Object arg2) {
		this.log.info(marker, format, arg1, arg2);
	}

	@Override
	public void info(final Marker marker, final String format, final Object... arguments) {
		this.log.info(marker, format, arguments);
	}

	@Override
	public void info(final Marker marker, final String msg, final Throwable t) {
		this.log.info(marker, msg, t);
	}

	@Override
	public boolean isWarnEnabled() {
		return this.log.isWarnEnabled();
	}

	@Override
	public void warn(final String msg) {
		this.log.warn(msg);
	}

	@Override
	public void warn(final String format, final Object arg) {
		this.log.warn(format, arg);
	}

	@Override
	public void warn(final String format, final Object... arguments) {
		this.log.warn(format, arguments);
	}

	@Override
	public void warn(final String format, final Object arg1, final Object arg2) {
		this.log.warn(format, arg1, arg2);
	}

	@Override
	public void warn(final String msg, final Throwable t) {
		this.log.warn(msg, t);
	}

	@Override
	public boolean isWarnEnabled(final Marker marker) {
		return this.log.isWarnEnabled(marker);
	}

	@Override
	public void warn(final Marker marker, final String msg) {
		this.log.warn(marker, msg);
	}

	@Override
	public void warn(final Marker marker, final String format, final Object arg) {
		this.log.warn(marker, format, arg);
	}

	@Override
	public void warn(final Marker marker, final String format, final Object arg1, final Object arg2) {
		this.log.warn(marker, format, arg1, arg2);
	}

	@Override
	public void warn(final Marker marker, final String format, final Object... arguments) {
		this.log.warn(marker, format, arguments);
	}

	@Override
	public void warn(final Marker marker, final String msg, final Throwable t) {
		this.log.warn(marker, msg, t);
	}

	@Override
	public boolean isErrorEnabled() {
		return this.log.isErrorEnabled();
	}

	@Override
	public void error(final String msg) {
		this.log.error(msg);
	}

	@Override
	public void error(final String format, final Object arg) {
		this.log.error(format, arg);
	}

	@Override
	public void error(final String format, final Object arg1, final Object arg2) {
		this.log.error(format, arg1, arg2);
	}

	@Override
	public void error(final String format, final Object... arguments) {
		this.log.error(format, arguments);
	}

	@Override
	public void error(final String msg, final Throwable t) {
		this.log.error(msg, t);
	}

	@Override
	public boolean isErrorEnabled(final Marker marker) {
		return this.log.isErrorEnabled(marker);
	}

	@Override
	public void error(final Marker marker, final String msg) {
		this.log.error(marker, msg);
	}

	@Override
	public void error(final Marker marker, final String format, final Object arg) {
		this.log.error(marker, format, arg);
	}

	@Override
	public void error(final Marker marker, final String format, final Object arg1, final Object arg2) {
		this.log.error(marker, format, arg1, arg2);
	}

	@Override
	public void error(final Marker marker, final String format, final Object... arguments) {
		this.log.error(marker, format, arguments);
	}

	@Override
	public void error(final Marker marker, final String msg, final Throwable t) {
		this.log.error(marker, msg, t);
	}

	private ThreadPoolExecutor createBackgroundExecutor(final String name) {
		ThreadPoolExecutor exec = new ThreadPoolExecutor(64, 64, 10L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(),
				r -> new Thread(r, name + " Background Thread"));
		exec.allowCoreThreadTimeOut(true);
		return exec;
	}

	private UUID doSubmit(final Runnable task, final boolean canCancel) {

		if (this.closed) {
			return null;
		}

		UUID uuid = canCancel ? UUID.randomUUID() : null;
		Runnable safeTask = this.safelyDispatch(task, canCancel ? uuid : null);

		this.doRun(safeTask, uuid);

		return uuid;
	}

	private void doRun(final Runnable safeTask, final UUID uuid) {

		ScheduledExecutorService executor = this.executor;

		if (uuid != null) {
			Future<?> future = executor.submit(safeTask);
			this.futures.put(uuid, future);
		} else {
			executor.execute(safeTask);
		}
	}

	private void doSchedule(final long delay, final TimeUnit unit, final Runnable safeTask, final UUID uuid) {

		Objects.requireNonNull(uuid);

		ScheduledExecutorService executor = this.executor;

		ScheduledFuture<?> future = executor.schedule(safeTask, delay, unit);
		this.futures.put(uuid, future);
	}

	private void doScheduleAtFixedRate(final long initialDelay, final long delay, final TimeUnit unit,
			final Runnable safeTask, final UUID uuid) {

		Objects.requireNonNull(uuid);

		ScheduledExecutorService executor = this.executor;

		ScheduledFuture<?> future = executor.scheduleAtFixedRate(safeTask, initialDelay, delay, unit);
		this.futures.put(uuid, future);
	}

	private void doScheduleWithFixedDelay(final long initialDelay, final long delay, final TimeUnit unit,
			final Runnable safeTask, final UUID uuid) {

		Objects.requireNonNull(uuid);

		ScheduledExecutorService executor = this.executor;

		ScheduledFuture<?> future = executor.scheduleWithFixedDelay(safeTask, initialDelay, delay, unit);
		this.futures.put(uuid, future);
	}

	private void shutdown(final ExecutorService executor) {

		executor.shutdown();

		boolean shutdown;
		try {
			shutdown = executor.awaitTermination(5, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			shutdown = false;
		}
		if (!shutdown) {
			this.log.warn("Actor '{}' did not shut down within 5 seconds. Shutting it down forcefully!", this.name);
			List<Runnable> uncompleted = executor.shutdownNow();
			this.log.warn("The following tasks did not complete: {}", uncompleted);
		}
	}

	private Runnable safelyDispatch(final Runnable task, final UUID uuid) {

		return () -> this.dispatcher.execute(() -> {
			try {
				task.run();
			} catch (Throwable th) {
				this.log.error("Uncaught exception in actor '" + this.name + "':", th);
			} finally {
				if (uuid != null) {
					if (!this.closed) {
						this.futures.remove(uuid);
					}
				}
			}
		});
	}

	private Runnable safelyWrap(final Runnable task) {

		return () -> {
			try {
				task.run();
			} catch (Throwable th) {
				this.log.error("Uncaught exception in actor '" + this.name + "':", th);
			}
		};
	}
}
